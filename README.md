# Virtual-Reception Rasa Chatbot

## Description
Virtual Reception Chatbot created with [Rasa Open Source](https://rasa.com/open-source/) and [Rasa-X](https://rasa.com/rasa-x/)

## Initial Build & Setup
```
docker build -t rasa .
docker-compose up
```

## Rasa Model Training
To use the chatbot, it first has to be trained
A new model can either be trained through the Rasa-X UI or CLI:
```
rasa train
```
This will create a new model located in the models folder. To activate the model, navigate to the models page on the Rasa-X UI, and mark the desired model as active.