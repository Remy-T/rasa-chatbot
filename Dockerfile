FROM python:3.7.7-stretch AS BASE

WORKDIR /app
RUN pip install rasa
RUN pip install rasa-x --extra-index-url https://pypi.rasa.com/simple
ENV PYTHONPATH=./src

ADD config.yml config.yml
ADD domain.yml domain.yml
ADD credentials.yml credentials.yml
ADD endpoints.yml endpoints.yml