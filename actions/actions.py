# This files contains custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions

import requests
import json
import pathlib
from typing import Any, Text, Dict, List
from rasa_sdk.events import SlotSet, FollowupAction, AllSlotsReset
from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.types import DomainDict

names = pathlib.Path("actions/names.txt").read_text().split("\n")

class ActionCheckReservation(Action):

    def name(self) -> Text:
        return "action_check_reservation"

    def run(self, dispatcher, tracker, domain):
            # obtain slot information.
            visitor_name = tracker.get_slot("visitor_name")
            contact_person_name = tracker.get_slot("contact_person_name")
            organisation_name = tracker.get_slot("organisation_name")

            init_contact_person_name = tracker.get_slot("init_contact_person_name")
            init_organisation_name = tracker.get_slot("init_organisation_name")

            latest_intent = tracker.latest_message['intent'].get('name')
            if "stop_active_conversation" in latest_intent: # If the latest intent was stop, deactive the form.
                return [AllSlotsReset()]

            # Check to see if the variables are filled and the reservation exists.
            if all(v is not None for v in [visitor_name, init_contact_person_name]):
                request = json.loads(requests.post('http://192.168.178.36:3000/visit/find', data={"visitor": visitor_name, "contact_person": init_contact_person_name}).text)  # make an api call

                # Check whether or not request returns data.
                if not request:
                    # dispatcher.utter_message(response = "utter_ask_input_confirmation", visitor_name = visitor_name, contact_person_name = init_contact_person_name)
                    # CHECK IF USERINPUT IS CORRECT.
                    dispatcher.utter_message(response = "utter_appointment_not_found")
                    # return []
                else:
                    dispatcher.utter_message(response = "utter_appointment_found")

            # Clear all the slots.
            return [AllSlotsReset()]

class ValidateAppointmentForm(FormValidationAction):
    def name(self) -> Text:
        return "validate_appointment_form"
    
    def validate_contact_person_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]:
        """Validate `contact_person_name` value."""
        
        latest_intent = tracker.latest_message['intent'].get('name')
        if "stop_active_conversation" in latest_intent: # If the latest intent was stop, deactive the form.
            return {"requested_slot":None} 

        # if slot_value not in names: # If the input is invalid, ask to re-enter.
        #     dispatcher.utter_message(response = "utter_invalid_contact_person_name")
        #     return{"contact_person_name": None}

        init_contact_person_name = tracker.get_slot("init_contact_person_name")
        if not init_contact_person_name: # If the init_contact_person_name is not set, set it for the first time and return it.
            return{"contact_person_name": slot_value, "init_contact_person_name": slot_value}  

        return{"contact_person_name": slot_value} # init_slot has already been set. just override main slot. 

    def validate_organisation_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]: 
        """Validate `organisation_name` value."""

        latest_intent = tracker.latest_message['intent'].get('name')
        if "stop_active_conversation" in latest_intent: # If the latest intent was stop, deactive the form.
            return {"requested_slot":None} 
        
        init_organisation_name = tracker.get_slot("init_organisation_name")
        if not init_organisation_name: # If the init_organisation_name is not set, set it for the first time and return it.
            return{"organisation_name": slot_value, "init_organisation_name": slot_value}  

        return{"organisation_name": slot_value}   

    def validate_time(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]: 
        """Validate `time` value."""

        latest_intent = tracker.latest_message['intent'].get('name')
        if "stop_active_conversation" in latest_intent: # If the latest intent was stop, deactive the form.
            return {"requested_slot":None} 

        return{"time": slot_value}  

    def validate_visitor_name(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: DomainDict,
    ) -> Dict[Text, Any]: 
        """Validate `visitor_name` value."""

        latest_intent = tracker.latest_message['intent'].get('name')
        if "stop_active_conversation" in latest_intent: # If the latest intent was stop, deactive the form.
            return {"requested_slot":None} 
        
        # if slot_value not in names: # If the input is invalid, ask to re-enter.
        #     dispatcher.utter_message(response = "utter_invalid_visitor_name")
        #     return{"visitor_name": None}

        return{"visitor_name": slot_value}

        

    class ActionHandleConfirmation(Action):

        def name(self) -> Text:
            return "action_handle_confirmation"

        def run(self, dispatcher, tracker, domain):
            dispatcher.utter_message(Text = "Hello World")
            
            return []
